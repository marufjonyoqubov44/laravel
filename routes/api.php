<?php

use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::apiResource('api', 'CategoryController@handle');
Route::get('subject', 'SubjectController@index');
Route::post('subject', 'SubjectController@save');
// Route::get('subject/{id}', 'SubjectController@index');
Route::Delete('/api/{id}', 'CategoryController@delete');
