<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;



Route::get('/', function () {
    return view('welcome');
});
Route::get('table', function () {
    if(isset($_GET['id'])){
        $cat = Category::find($_GET['id']);
        return $cat;
    }
    $cats = Category::get();
    return view('table',[
        'category'=> $cats
    ]);
});
Route::post('table', function () {
    if(isset($_POST['id'])){
        $cat = Category::find($_POST['id']);
        $cat->name = $_POST['name'];
        $cat->status = $_POST['status'];
        $cat->save();
        $cats = Category::get();
        return view('table', [
            'category' => $cats
        ]);
    }
    $cat = new Category;
    $cat->name = $_POST['name'];
    $cat->status = $_POST['status'];
    $cat->save();
    $cats = Category::get();
    return view('table', [
        'category' => $cats
    ]);
});
