<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public   $rules = [
        'name' => 'required|max:20',
        'status' => 'required',
    ];
    protected $fillable = ['status', 'name','by','updated_at','created_at'];   
}
