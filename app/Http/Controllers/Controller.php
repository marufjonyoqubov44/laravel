<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function checkBody(){

    }
    protected function error($type,$message)
    {
       return abort(response()->json("$message", $type));
    }
    protected function create($model,$res)
    {
        $validator = FacadesValidator::make($res, $model->rules);
        if ($validator->fails()) {
            $this->error(429, "Bazaga noto'g'ri so'rov");
        }
        $model::create($res);
        return true;
    }
    protected function update($model, $res)
    {
        $validator = FacadesValidator::make($res, $model->rules);
        if ($validator->fails()) {
            $this->error(429, "Bazaga noto'g'ri so'rov");
        }
        $model->fill($res);
        $model->save();
        return true;
    }

}
