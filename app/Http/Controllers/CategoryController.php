<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function index(){
        $users = DB::table('categories')
            // ->select(DB::raw('count(*) as count, parent_id'))
            // ->where('parent_id', '', 2)
            // ->groupBy('parent_id')
            // ->groupBy('id')
            ->get();
        return $users;
    }
}
