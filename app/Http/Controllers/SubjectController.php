<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as FacadesValidator;

class SubjectController extends Controller
{
    public function index(Request $request){
        $id = $request->input('id');
        if($id){
            return $id;
        }
        $subject = Subject::paginate(2);
        $pagenate= $subject->items();
        $count = $subject->total();
         $response=[
             'count'=>$count,
             'value'=>$pagenate
         ];
        // $subject = Subject::where('id','>',0)->chunk(1,function ($users) {
        //     foreach ($users as $user) {
        //         DB::table('subjects')
        //             ->update(['by' => 0]);
        //     }
        // });
        //   $subject = Subject::selectRaw('status, count(*) as total')
        //              ->where('status', '>=', 1)
        //              ->groupBy('status')
        //              ->having('total','>',1)
        //              ->get();
      return $subject;
    }
    public function save(Request $request){
        $data = json_decode($request->getContent(), true);
        $token = ($request->header())['token'];
        if (isset($data['id'])) {
            $model = Subject::find($data['id']);
            if($model){
              $this->update($model,$data);
              return 1;
            }
            $this->error(400,"Bunday id lik fan topilmadi");
            return "update";
        } 
        $model = new Subject;
        $this->create($model, $data);
    }
}
